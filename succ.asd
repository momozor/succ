(defsystem "succ"
  :version "0.1.0"
  :author "Momozor"
  :license "MIT"
  :depends-on ("alexandria"
               "uiop"
               "prove")
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description "Behavior Driven Development framework for Common Lisp"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "succ/tests"))))

(defsystem "succ/tests"
  :author "Momozor"
  :license "MIT"
  :depends-on ("succ"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for succ"

  :perform (test-op (op c) (symbol-call :rove :run c)))
