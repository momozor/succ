(defpackage succ/tests/main
  (:use :cl
        :succ
        :rove))
(in-package :succ/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :succ)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
