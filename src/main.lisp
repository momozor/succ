(defpackage succ
  (:use :cl))
(in-package :succ)

#|
(succ:sdescribe "a module to sort Cassini space probe data"
  (succ:sit "function adder should return 5"
    (= (adder 1) 5))
  (succ:sit "function remover should return 4"
    (= (remover 1) 4)))
|#
